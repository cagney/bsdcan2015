What happens when a DWARF and a Daemon start dancing by the light of the silvery moon?
======================================================================================


Presented at [BSDCan](http://www.bsdcan.org/2015/schedule/events/561.en.html).  The [slides](http://www.bsdcan.org/2015/schedule/attachments/328_2015-BSDCan_NetBSD-Lua-DWARF.pdf) are available for download; and, of course, it is on [youtube](http://www.youtube.com/watch?v=XDIcD4LR5HE).

Updates
-------

Occasionally stuff is missed or isn't correct.  Here's some additional information:

- FreeBSD has a new [DWARF reader](http://sourceforge.net/p/elftoolchain/wiki/Home/); this goes on my list of possibilities

- there's an alternative debug format called [CTF](https://wiki.freebsd.org/SummerOfCode2014/CTF_implementation_and_use_in_the_kernel_debugger); and a tool to translate DWARF into this format.  Part of the motivation for the new format is to get the kernel and trace tools using it.


Getting things Working
======================

Building
--------

    ./debug-tools.sh
    ./debug-kernl.sh
    ./debug-boot.sh

Running
-------

At "root device:" prompt, type: ddb

At "db>" prompt, type: lua

At "lua>" prompt, type: ddb=require("ddb")

Source Code
-----------

sys/ddb/db_lua.c: contains the lua bindings

frawd: contains the actual lua code

Licence
-------

Variants of BSD, MIT, ...