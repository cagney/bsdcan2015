#include <sys/cdefs.h>

#ifdef _KERNEL_OPT
#include "opt_ddbparam.h"
#endif

#include <sys/param.h>
#include <sys/proc.h>
#include <sys/cpu.h>
#include <sys/malloc.h>

#include <ddb/ddb.h>

#include <ddb/db_lua.h>

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

static void *
lua_alloc (void *ud, void *ptr, size_t osize, size_t nsize)
{
	if (nsize == 0 && ptr != NULL) {
		//printf("lua_alloc free %p", ptr);
		free(ptr, M_TEMP);
		ptr = NULL;
	} else if (ptr == NULL && nsize > 0) {
		//printf("lua_alloc malloc %zd", nsize);
		ptr = malloc(nsize, M_TEMP, M_WAITOK);
	} else if (ptr != NULL && nsize > 0) {
		//printf("lua_alloc realloc %p of size %zd", ptr, nsize);
		ptr = realloc(ptr, nsize, M_TEMP, M_WAITOK);
	} else {
		//printf("lua_alloc ignore ptr %p nsize %zd", ptr, nsize);
	}
	//printf("; return %p\n", ptr);
	return ptr;
}

extern char db_lua_buf[]; //XXX:
extern size_t db_lua_buf_size;

static int
load(lua_State *L)
{
	const char *module = lua_tostring(L, -1);
	char *p = db_lua_buf;
	while (p < &db_lua_buf[db_lua_buf_size] && strcmp(p, module) != 0) {
		// skip name
		p = p + strlen(p) + 1;
		// skip code
		p = p + strlen(p) + 1;
	}
	if (strcmp(p, module) != 0) {
		lua_pushliteral(L, "module not found");
		return lua_error(L);
	}

	// skip name
	p = p + strlen(p) + 1;

	// on code
	size_t sz = strlen(p);
	luaL_loadbuffer(L, p, sz, module);

	// now need to execute that code so it can "load" the module;
	// pass the module its name (normally it would also get the
	// file).
	lua_pushstring(L, module);
	lua_call(L, 1, 1);

	// return what ever module left on the stack.
	return 1;
}

static int
require(lua_State *L)
{
	const char *module = lua_tostring(L, -1);
	printf("requiring module %s\n", module);
	luaL_requiref(L, module, load, 0);
	return 1;
}

static int
peek(lua_State *L)
{
	int n = lua_gettop(L);
	if (n != 1) {
		printf("%d wrong\n", n);
		return luaL_error(L, "expecting one argument");
	}

	int isint;
	size_t addr = (size_t) lua_tointegerx(L, 1, &isint);
	if (!isint) {
		printf("expecting number\n");
		return luaL_error(L, "expecting one argument");
	}

	uint8_t byte;
	//printf("%zx/%zu", addr, addr);
	db_read_bytes(addr, sizeof(byte), &byte);
	//printf(" %02x/%u\n", byte, byte);
	lua_pushinteger(L, byte);
	return 1;
}

extern char db_debug_buf[]; // XXX
extern size_t db_debug_buf_size; // XXX

void
db_lua_cmd(db_expr_t addr, bool have_addr, db_expr_t count, const char *modif)
{
	lua_State *L = lua_newstate(lua_alloc, NULL/*ud*/);   /* opens Lua */

	luaL_openlibs(L);

	lua_register(L, "require", require);
	lua_register(L, "db_peek", peek);
	lua_pushinteger(L, (lua_Integer)(size_t)&db_debug_buf);
	lua_setglobal(L, "db_debug_buf");
	lua_pushinteger(L, (lua_Integer)db_debug_buf_size);
	lua_setglobal(L, "db_debug_buf_size");
	
	printf("Starting lua, enter an empty line to exit\n");
	char buf[256];
	while (1) {

		size_t i = db_readline3("lua", buf, sizeof(buf) - 1);
		if (i <= 1)
			break;

		int error = luaL_loadbuffer(L, buf, i, "line") ||
			lua_pcall(L, 0, 0, 0);
		
		if (error) {
			printf("%s\n", lua_tostring(L, -1));
			lua_pop(L, 1);  /* pop error message from the stack */
		}
	}
    
	lua_close(L);
}
