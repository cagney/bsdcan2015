#define		LUA_FILLER	"|This is DB_LUA information!"

#ifdef COPY_LUA
#ifndef LUA_SPACE
char		db_lua_buf[] = LUA_FILLER;
#else
char		db_lua_buf[LUA_SPACE] = LUA_FILLER;
#endif
int		db_lua_buf_size = sizeof(db_lua_buf);
#endif
