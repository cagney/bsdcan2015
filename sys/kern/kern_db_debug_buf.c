#define		DEBUG_FILLER	"|This is DEBUG information!"

#ifdef COPY_DEBUG
#ifndef DEBUG_SPACE
char		db_debug_buf[] = DEBUG_FILLER;
#else
char		db_debug_buf[DEBUG_SPACE] = DEBUG_FILLER;
#endif
int		db_debug_buf_size = sizeof(db_debug_buf);
#endif
