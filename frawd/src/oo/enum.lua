-- an enum is a combinatin of values and a lookup table

local enum = {}

function enum:__tostring()
   if self.description then
      return self.description
   else
      return self.name
   end
end

return function(table)
   local o = {}
   for k, v in pairs(table) do
      o[k] = v
      o[v.value] = v
      setmetatable(v, enum)
   end
   return o
end
