-- Boiler plate class object.

-- To override new create a factory file/class.

local class = {}
class.__index = class

function class:new(o)
   o = o or {}
   -- this both does a sanity check and ensures that objects do not
   -- call new
   assert(self.__index)
   setmetatable(o, self)
   return o
end

return function(o)
   o = o or {}
   o.__index = o
   -- make new available
   setmetatable(o, class)
   return o
end
