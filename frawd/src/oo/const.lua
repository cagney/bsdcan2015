-- a const is a collection of integers

local const = {}

function const:__tostring()
   if self.description then
      return self.description
   else
      return self.name
   end
end

return function(table)
   local o = {}
   for k, v in pairs(table) do
      o[k] = v.value
      o[v.value] = v
      setmetatable(v, const)
   end
   return o
end
