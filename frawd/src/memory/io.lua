-- little endian io operations

local Class = require("oo/class")

local io = Class{
   offset = 0
}

function io:seek(offset)
   local old_offset = self.offset
   if offset then
      if offset >= 0 then
	 self.offset = offset
      else
	 self.offset = #self - offset
      end
   end
   return old_offset
end

function io:string()
   local string = self.memory:string(self.offset)
   self.offset = self.offset + #string + 1
   return string
end

function io:__len()
   return #self.memory
end

function io:map(start, length)
   local o = {
      memory = self.memory:map(start, length),
      read = read
   }
   setmetatable(o, getmetatable(self))
   return o
end

return function(memory)
   local o = { memory = memory }
   setmetatable(o, io)
   return o
end
