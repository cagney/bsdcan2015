-- little endian io operations

local IO = require("memory/io")

function read(self, length)
   local value = 0
   for i = length-1, 0, -1 do
      value = (value << 8) | self.memory[self.offset + i]
   end
   self.offset = self.offset + length
   return value
end

return function(memory)
   local o = IO(memory)
   o.read = read
   return o
end
