local Class = require("oo/class")

local string = Class{}

function string:__tostring()
   -- try the cache
   if self.cache then
      return self.cache
   end
   -- read from memory
   local s = {}
   local i = self.offset
   while true do
      local c = self.memory[i]
      if c == 0 then
	 break
      end
      s[#s + 1] = _G.string.char(c)
      i = i + 1
   end
   self.cache = table.concat(s)
   return self.cache
end

function string:__len()
   return #tostring(self)
end

function string:__eq(rhs)
   print("__eq", self, rhs)
   return tostring(self) == tostring(rhs)
end

return function(memory, offset)
   local o = {
      memory = memory,
      offset = offset
   }
   setmetatable(o, string)
   return o
end
