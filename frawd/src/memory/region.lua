-- slice and dice a region backed by an in-memory string

-- should abstract much of this into oo/array as the oo/class trick
-- doesn't work with metamethods like __index - the wrong table gets
-- passed in.

local String = require("memory/string")

local region = { }

function region.__index(table, index)
   if type(index) == "number" then
      assert(index >= 0)
      assert(index < table.length)
      local byte = table.peek(table.start + index)
      return byte
   else
      -- so foo.string works
      local methods = getmetatable(table)
      return methods[index]
   end
end

function region:__setindex(index, value)
   assert()
end

function region:__len()
   return self.length
end

function region:string(offset)
   return String(self, offset)
end

local function map(peek, poke, start, length)
   local o = {
      peek = peek,
      poke = poke,
      start = start,
      length = length
   }
   setmetatable(o, region)
   return o
end

function region:map(start, length)
   return map(self.peek, self.poke, start, length)
end

return function(peek, poke, start, length)
   return map(peek, poke, start, length)
end
