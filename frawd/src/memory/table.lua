-- a table backed by memory; access using []

local Class = require("oo/class")

local table = Class{}

-- number of entries
function table:__len()
   return self.count
end

function table:__index(index)
   -- try the cache first
   local entry
   entry = self.cache[index]
   if entry then
      return entry
   end

   if type(index) == "number" then
      if index < 0 or index >= #self then
	 return nil
      end
      self.region:seek(index * self.size)
      entry = self:build(self.region)
   else
      entry = self:find(index)
   end
   
   self.cache[index] = entry
   return entry
end

function table:__setindex(index, value)
   assert()
end

function table:__pairs()
   return function(state, i)
      i = i + 1
      local entry = state[i]
      if entry == nil then
	 return nil
      else
	 return i, entry
      end
   end, self, -1
end

return function(region, start, count, size,  build, find)
   local o = {
      region = region:map(start, count * size),
      build = build,
      find = find,
      count = count,
      size = size,
      cache = {}
   }
   setmetatable(o, table)
   return o
end
