-- slice and dice a file into a region

local MemoryRegion = require("memory/region")

return function(filename)
   local file = assert(io.open(filename, "rb"))
   local buffer = file:read("*a");
   file:close()
   local peek = function(addr)
      return buffer:byte(addr + 1)
   end
   return MemoryRegion(peek, nil, 0, #buffer)
end
