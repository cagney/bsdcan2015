-- slice and dice a file into a region

local Region = require("memory/region")
local Elf = require("elf/elf")
local Dwarf = require("dwarf/dwarf")

return function()
   local ddb = {}
   ddb.debug = Region(function(addr)
			 return db_peek(db_debug_buf + addr)
		      end,
		      nil,
		      0, db_debug_buf_size)
   ddb.elf = Elf(ddb.debug)
   ddb.dwarf = Dwarf(ddb.elf)
   return ddb
end
