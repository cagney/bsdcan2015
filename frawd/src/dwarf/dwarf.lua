local Class = require("oo/class")
local PubNames = require("dwarf/pubnames")

local dwarf = Class{ }

return function(elf)
   -- memory and registers?
   local o =  { elf = elf }
   setmetatable(o, dwarf)

   o.pubnames = PubNames(elf)
   return o
end
