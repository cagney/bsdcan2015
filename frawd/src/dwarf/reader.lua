local Class = require("oo/class")

local reader = Class{}

function reader:initial_length()
   local length = self.file:word(4)
   if length == 0xffffffff then
      length = self.file:xword()
   end
   return length
end

function reader:uhalf()
   return self.file:half()
end

function reader:section_offset()
   return self.file:word()
end

function reader:section_length()
   return self.file:word()
end

function reader:string()
   return self.file:string()
end

function reader:seek(offset)
   return self.file:seek(offset)
end

function reader:__len()
   return #self.file
end

return function(section)
   local o = {
      file = section.elf.file:map(section.offset, section.size)
   }
   setmetatable(o, reader)
   return o
end
