local Reader = require("dwarf/reader")

local pubnames = {}

function pubnames:__index(index)
   local reader = Reader(self.section)
   local names = {}
   print(reader:seek(), #reader)
   while reader:seek() < #reader do
      local initial_length = reader:initial_length()
      local version = reader:uhalf()
      local debug_info_offset = reader:section_offset()
      local debug_info_length = reader:section_length()
      print("pubname", initial_length, version, debug_info_offset, debug_info_length)
      while true do
	 local offset = reader:section_offset()
	 if offset == 0 then
	    break
	 end
	 local name = reader:string()
	 -- XXX: __eq doesn't with table==string
	 if tostring(name) == index then
	    print(index, name, debug_info_offset, offset)
	    names[#names + 1] = {
	       cu = debug_info_offset,
	       die = offset
	    }
	 end
      end
   end
   return names
end

return function(elf, section)
   local o = {
      -- XXX: pubnames["section"] will return this
      section = section or elf.shdr[".debug_pubnames"]
   }
   setmetatable(o, pubnames)
   return o
end
