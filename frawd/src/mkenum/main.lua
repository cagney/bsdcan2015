local Input = require("mkenum/input")
local Parse = require("mkenum/parse")

local table = Parse(Input())
local pad = "   "

local function dump(table, nest)
   for k, v in pairs(table) do
      print(nest .. "-- " .. k .. ": " .. tostring(v))
   end
end

local function print_entry(table, nest)
   print(nest .. table.name .. " = {")
   for k, v in pairs(table) do
      if k ~= "name" then
	 print(nest .. pad .. k .. " = " .. v .. ",")
      else
	 print(nest .. pad .. k .. " = \"" .. v .. "\",")
      end
   end
   print(nest .. "},")
end

local function print_table(table, nest, tail)
   print(nest .. table.name .. " = " .. table.class .. "{")
   for i, v in ipairs(table.entries) do
      dump(v, nest .. pad)
      if v.class == nil then
	 print_entry(v, nest .. pad)
      elseif v.class == "const" then
	 print(nest .. pad .. v.name .. " = " .. v.value .. ",")
      else
	 print_table(v, nest .. pad, ",")
      end
   end
   print(nest .. "}" .. tail)
end

print("local Enum = require(\"oo/enum\")")
print("local Set = require(\"oo/set\")")
print("local Const = require(\"oo/const\")")
print("local Module = require(\"oo/module\")")
print()
print_table(table, "", "")
print()
print("return " .. table.name)
