local assert = require("mkenum/assert")
local Class = require("oo/class")

input = Class{
  line, name, value, desc = nil
}

-- Convert a single-quoted string (or integer) to an integer string

function input:ivalue()
  if self.value:match("'") then
    local v = self.value:match("'([^'])'")
    return tostring(v:byte())
  else
    return self.value
  end
end

assert:equals(input.ivalue({value = "'E'"}), "69")
assert:equals(input.ivalue({value = "10"}), "10")
assert:equals(input.ivalue({value="0x10"}), "0x10")

function input:split()
   local desc
   self.name, self.value, desc = self.line:match("^(%S*)%s*(%S*)%s*(.*)$")
   if desc and desc ~= "" then
      self.desc = desc
   else
      self.desc = nil
   end
   return self
end

function input:getln()
  while true do
    self.line = io.stdin:read()
    self.count = self.count + 1
    if not self.line then
      return self
    end
    -- io.stderr:write(self.count .. "\t" .. self.line .. "\n")
    if not self.line:match("^%s*$") then
      self:split()
      return self	
    end
  end
end

function input:eof()
  return self.line == nil
end

function input:error(message)
   error("Syntax error at line " .. self.count .. ", " .. message .. "\n" ..
	    "\tline=[[" .. self.line .."]]\n" ..
	    "\tname=[[" .. self.name .."]]\n" ..
	    "\tvalue=[[" .. self.value .. "]]")
end

local function new(o)
  local n = input:new(o)
  n.count = 0
  if o then
    n:split()
  else
    n:getln()
  end
  return n
end

do
  local input = new{line = "a b c"}
  assert:equals(input.name, "a")
  assert:equals(input.value, "b")
  assert:equals(input.desc, "c")
end

return new
