-- Assertion / testing.

local assert = {}

-- Compare to strings providing lots of information

local function compare(lhs, rhs)
  if lhs:len() ~= rhs:len() then
    return false, "[[".. lhs .. "]] and [[" .. rhs .. "]] have different lengths"
  end
  for l = 1, lhs:len() do
    if lhs:byte(l) ~= rhs:byte(l) then
      return false, "[[".. lhs .. "]] and [[" .. rhs .. "]] have different characters at position " .. l
    end
  end
  return lhs == rhs, "compare"
end

function assert:equals(lhs, rhs)
  status, err = compare(lhs, rhs)
  if not status then
    _G.error(err)
  end
end

function assert:notEquals(lhs, rhs)
  status, err = compare(lhs, rhs)
  if status then
    error(err)
  end
end

do
  assert:equals("a", "a")
  assert:notEquals("a", "ab")
  assert:notEquals("a", "b")
end

return assert
