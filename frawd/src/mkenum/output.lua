-- Indented output 

local Class = require("oo/class")
local assert = require("mkenum/assert")

local indent = Class{
  tab = "  ",
  indentation = ""
}

function indent:write(...)
  for _, v in ipairs{...} do
    self:put(v)
  end
end

function indent:writeln(...)
  self:write(...)
  self:write("\n")
end

function indent:print(...)
  self:write(self.indentation)
  self:write(...)
  return self
end

function indent:println(...)
  self:write(self.indentation)
  self:writeln(...)
  return self
end

function indent:nest(code)
  local indentation = self.indentation
  self.indentation = self.indentation .. self.tab
  code()
  self.indentation = indentation
end

local output = {}

function output:buffer()
  return indent:new{buffer = "", put = function(self, v)
    self.buffer = self.buffer .. v
  end}
end

function output:stream(f)
  return indent:new{stream = f, put = function(self, v)
    self.stream:write(v)
  end}
end

-- assert that the result of calling func(output,arg) is as expected

function output:assert_equals(func, arg, result)
  buffer = output:buffer()
  func(buffer, table.unpack(arg))
  assert:equals(buffer.buffer, result)
end

do
  assert:equals(output:buffer():println("Hello World!").buffer, [[
Hello World!
]])
  output:assert_equals(function(output, a, b, c)
    output:println(a, b, c)
  end, {"a", "b", "c"}, "abc\n")
end

return output
