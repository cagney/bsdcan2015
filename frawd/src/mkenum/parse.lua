local Prefix = require("mkenum/prefix")
local assert = require("mkenum/assert")
local output = require("mkenum/output")
local Class = require("oo/class")

function parse_entry(input, prefix, entries, tables)
   if input.line:match("^#") then
      -- # ...
      return nil
   elseif input.name == "@reserved" then
      -- @reserved value description
      return nil
   end
   local description = nil
   if input.desc then
      description = "\"" .. input.desc .. "\""
   end
   if input.name:match(".*|.*") then
      -- name|alias value
      name, alias = input.name:match("([^|]*)|([^|]*)")
      entries[#entries + 1] = {
	 name = prefix:tangle(name),
	 alias = prefix:tangle(alias),
	 value = input.value,
	 description = description
      }
   elseif input.name:match("^&.*") then
      -- &mask value
      mask = input.name:match("&(.*)")
      entries[#entries + 1] = {
	 name = prefix:tangle(mask),
	 value = input:ivalue(),
	 description = description
      }
      -- global as well
      tables[#tables + 1] = {
	 class = "const",
	 name = mask,
	 value = input:ivalue()
      }
   elseif input.name:match(".+&.+") then
      -- name&mask value
      name, mask = input.name:match("(.+)&(.+)")
      return { name = name, mask = mask, value = input.value }
   elseif input.name:match(":.*") then
      -- :name value
      name = input.name:match("^:%s*(.*)")
      return { name = name, value = input.value }
   elseif input.name:match(".*-.*") then
      -- name-name value-value
      name, lo_name, hi_name = input.name:match("^([^0-9]+)([0-9]+)-[^0-9]+([0-9]+)")
      suffix = tonumber(lo_name)
      lo_value, hi_value = input.value:match("([^-]+)-([^-]+)")
      for value = lo_value, hi_value do
	 -- mangled = print_entry(output, prefix, name .. suffix, value)
	 suffix = suffix + 1
      end
   elseif input.name == "@reserved" then
      -- output:println("-- ", input.value, " = ", input.desc)
   else
      -- name value description
      entries[#entries + 1] = {
	 name = prefix:tangle(input.name),
	 value = input.value,
	 description = description
      }
      -- global constant
      tables[#tables + 1] = {
	 class = "const",
	 name = input.name,
	 value = input.value
      }
   end
end

function parse_entries(input, prefix, table_entries, tables)
   input:getln()
   while input.name ~= "@end" do
      parse_entry(input, prefix, table_entries, tables)
      input:getln()
   end
end

function parse_table(input, prefix, class, tables)
   local name = prefix:trim(input.value)
   prefix:append(name)
   local table = { class = class, name = name, entries = {} }
   tables[#tables + 1] = table
   parse_entries(input, prefix, table.entries, tables)
end

function parse_body(input, prefix, tables)
   local classes = { ["@enum"] = "Enum", ["@set"] = "Set", ["@const"] = "Const" }
   input:getln()
   while input.name ~= "@end" do
      local class = classes[input.name]
      if not class then
	 input:error("@enum, @set, or @const expected")
      end
      parse_table(input, prefix, class, tables)
      input:getln()
   end
end

function parse_module(input)
   if input.name ~= "@module" then
      input:error("@module expected")
   end
   local table = {
      name = input.value,
      class = "Module",
      entries = {}
   }
   local prefix = Prefix(input.value)
   parse_body(input, prefix, table.entries)
   return table
end

return parse_module
