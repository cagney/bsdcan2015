-- strip any leading prefix off a name

local assert = require("mkenum/assert")
local Class = require("oo/class")

local prefix = Class{
  keyword = {
    ["and"] = 1,
    ["or"] = 1,
    ["not"] = 1,
    ["local"] = 1
  }
}

function prefix:append(s)
  self[#self + 1] = s
  return self
end

function prefix:trim(name)
  for i, v in ipairs(self) do
    name = name:gsub("^" .. v .. "[_]", "")
  end
  return name
end

function prefix:mangle(name)
  if name:match("^[0-9].*") then
    return "_" .. name
  elseif self.keyword[name] then
    return "_" .. name
  else
    return name
  end
end

function prefix:tangle(name)
  return self:mangle(self:trim(name))
end

local function new(s)
  o = prefix:new()
  if s then
    o:append(s)
  end  
  return o
end

do
  assert:equals(new("a"):trim("name"), "name")
  assert:equals(new("a"):trim("a_name"), "name")
  assert:equals(new("a"):append("b"):trim("a_b_name"), "name")
  assert:equals(new("a"):tangle("b"), "b")
  assert:equals(new("a"):tangle("not"), "_not")
  assert:equals(new("a"):tangle("a_1"), "_1")
end

return new
