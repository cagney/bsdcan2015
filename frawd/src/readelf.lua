#!/home/cagney/frawd/lua/lua

local MemoryFile = require("memory/file")
local Elf = require("elf/elf")
local Dwarf = require("dwarf/dwarf")
local String = require("memory/string")

for _, v in ipairs{...} do
   print(v)
   local file = MemoryFile(v)
   elf = Elf(file)
   for i, s in pairs(elf.shdr) do
      elf.strings:seek(s.name)
      print(i, tostring(elf.strings:string()), tostring(s.type))
   end
   for i, s in pairs(elf.phdr) do
      print(i, tostring(s.type))
   end
   dwarf = Dwarf(elf)
   for i, s in pairs(dwarf.pubnames["main"]) do
      print(s.cu, s.die)
   end
end
