local ELF = require("elf/ELF")
local Table = require("memory/table")

local class = {}

class[ELF.ELFCLASS.ELFCLASS32] = function(file)
  local o = {}
  o.type = file:word()
  o.offset = file:off()
  o.vaddr = file:addr()
  o.paddr = file:addr()
  o.filesz = file:word()
  o.memsz = file:word()
  o.flags = file:word()
  o.align = file:word()
  return o
end

class[ELF.ELFCLASS.ELFCLASS64] = function(file)
  local o = {}
  o.type = file:word()
  o.flags = file:word()
  o.offset = file:off()
  o.vaddr = file:addr()
  o.paddr = file:addr()
  o.filesz = file:xword()
  o.memsz = file:xword()
  o.align = file:xword()
  return o
end

return function(elf, start, count, size)
   return Table(elf.file, start, count, size,
		function(table, file)
		   local phdr = class[elf.class](file)
		   local type = ELF.PT[phdr.type]
		   if type then
		      phdr.type = type
		   end
		   phdr.elf = elf
		   return phdr
		end);
end
