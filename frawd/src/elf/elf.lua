local ELF = require("elf/ELF")
local Phdr = require("elf/phdr")
local Shdr = require("elf/shdr")
local File = require("elf/file")
local String = require("memory/string")

return function(memory)
   local elf = { }

   -- magic
   print(ELF.EI_MAG0)
   print(ELF.ELFMAG0)
   if memory[ELF.EI_MAG0] ~= ELF.ELFMAG0 or
      memory[ELF.EI_MAG1] ~= ELF.ELFMAG1 or
      memory[ELF.EI_MAG2] ~= ELF.ELFMAG2 or
      memory[ELF.EI_MAG3] ~= ELF.ELFMAG3 then
      error("bad magic number")
   end

   -- file class
   local class = memory[ELF.EI_CLASS]
   elf.class = ELF.ELFCLASS[class]
   if not elf.class then
      error("unknown class")
   end
   print("class:", tostring(elf.class))

   -- data encoding
   local data = memory[ELF.EI_DATA]
   elf.data = ELF.ELFDATA[data]
   if not elf.data then
      error("Unknown data representation")
   end
   print("data:", tostring(elf.data))

   -- file version
   print("file version:", memory[ELF.EI_VERSION])

   -- OSABI
   local osabi = memory[ELF.EI_OSABI]
   elf.osabi = ELF.ELFOSABI[osabi]
   if not elf.osabi then
      error("Unknown OSABI")
   end
   print("abi:", tostring(elf.osabi))

   --
   print("abi version:", memory[ELF.EI_ABIVERSION])

   elf.file = File(elf.class, elf.data, memory)

   elf.file:seek(ELF.EI_NIDENT)
   local type = elf.file:half()
   elf.type = ELF.ET[type]
   print("type:", tostring(elf.type))

   local machine = elf.file:half()
   elf.machine = ELF.EM[machine]
   print("machine:", tostring(elf.machine))

   local version = elf.file:word()
   elf.version = ELF.EV[version]
   print("version:", tostring(elf.version))

   elf.entry = elf.file:addr()
   print(string.format("entry: %x", elf.entry))
   print("entry:", elf.entry)

   elf.phoff = elf.file:off()
   print("phoff:", elf.phoff)
   elf.shoff = elf.file:off()
   print("shoff:", elf.shoff)

   elf.flags = elf.file:word()
   elf.ehsize = elf.file:half()

   elf.phentsize = elf.file:half()
   print("phentsize:", elf.phentsize)
   elf.phnum = elf.file:half()
   print("phnum:", elf.phnum)
   elf.shentsize = elf.file:half()
   print("shetnsize:", elf.shentsize)
   elf.shnum = elf.file:half()
   print("shnum:", elf.shnum)

   elf.shstrndx = elf.file:half()
   print("String index:", elf.shstrndx)

   elf.shdr = Shdr(elf, elf.shoff, elf.shnum, elf.shentsize)
   elf.phdr = Phdr(elf, elf.phoff, elf.phnum, elf.phentsize)

   elf.strings = elf.file:map(elf.shdr[elf.shstrndx].offset, elf.shdr[elf.shstrndx].size)

   return elf
end
