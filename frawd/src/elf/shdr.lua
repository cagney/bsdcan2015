local ELF = require("elf/ELF")
local Table = require("memory/table")
local String = require("memory/string")

local  class = {}

class[ELF.ELFCLASS.ELFCLASS32] = function(file)
   local o = {}
   o.name = file:word()
   o.type = file:word()
   o.flags = file:word()
   o.addr = file:addr()
   o.offset = file:off()
   o.size = file:word()
   o.link = file:word()
   o.info = file:word()
   o.addralign = file:word()
   o.entsize = file:word()
   return o
end

class[ELF.ELFCLASS.ELFCLASS64] = function(file)
   local o = {}
   o.name = file:word()
   o.type = file:word()
   o.flags = file:xword()
   o.addr = file:addr()
   o.offset = file:off()
   o.size = file:xword()
   o.link = file:word()
   o.info = file:word()
   o.addralign = file:xword()
   o.entsize = file:xword()
   return o
end

return function(elf, start, count, size)
   return Table(elf.file, start, count, size,
		function(table, file)
		   local shdr = class[elf.class](file)
		   shdr.type = ELF.SHT[shdr.type]
		   shdr.elf = elf
		   return shdr
		end,
		function(table, index)
		   for i, s in pairs(table) do
		      local name = tostring(String(elf.strings.io.memory, s.name))
		      if name == index then
			 return s
		      end
		   end
		   return nil
		end)
end
