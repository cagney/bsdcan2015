local ELF = require("elf/ELF")
local LittleIO = require("memory/little_io")
local BigIO = require("memory/big_io")
local Class = require("oo/class")

local file = Class{}

function file:seek(offset)
   return self.io:seek(offset)
end

function file:uchar()
   return self.io:read(1)
end

function file:half()
   return self.io:read(2)
end

function file:word()
   return self.io:read(4)
end

function file:xword()
   return self.io:read(8)
end

function file:sword()
   assert("not implemented")
end

function file:sxword()
   assert("not implemented")
end

function file:string()
   return self.io:string()
end

function file:__len()
   return #self.io
end

function file:map(start, length)
   local o = {
      io = self.io:map(start, length),
      addr = self.addr,
      off = self.off
   }
   setmetatable(o, file)
   return o
end

return function(class, data, memory)

   if data == ELF.ELFDATA.ELFDATA2LSB then
      io = BigIO(memory)
   end
   if data == ELF.ELFDATA.ELFDATA2LSB then
      io = LittleIO(memory)
   end

   local o = {
      io = io,
   }
   setmetatable(o, file)

   if class == ELF.ELFCLASS.ELFCLASS32 then
      o.addr = o.word
      o.off = o.word
   end
   if class == ELF.ELFCLASS.ELFCLASS64 then
      o.addr = o.xword
      o.off = o.xword
   end

   return o
end
