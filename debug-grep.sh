#!/bin/sh
find sys -name obj -type d -prune -o -type f -print | xargs grep "$@"
